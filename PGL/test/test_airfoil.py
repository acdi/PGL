import unittest
import numpy as np
from PGL.components.airfoil import AirfoilShape

class AirfoilShapeTests(unittest.TestCase):
    ''' Tests for components.airfoil.py
    '''
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.af = AirfoilShape()
        self.af.initialize(points=np.loadtxt('data/cylinder.dat'))
        # self.af.initialize(points=np.loadtxt('data/ffaw3480.dat'))
        self.dps_s01 = np.array([0.0, 0.25, 0.5, 0.75, 1.0])

    def test_s_to_11_10(self):

        dps_s11 = np.array([self.af.s_to_11(s) for s in self.dps_s01])
        dps_s01n = np.array([self.af.s_to_01(s) for s in dps_s11])

        self.assertEqual(np.testing.assert_allclose(dps_s01n, self.dps_s01, 1E-06), None)

    def test_s_to_11_10_rotate(self):

        afi = self.af.copy()
        afi.rotate_z(-45.0)
        dps_s11 = np.array([afi.s_to_11(s) for s in self.dps_s01])
        afii = self.af.copy()
        afii.rotate_z(+45.0)
        dps_s01n = np.array([afii.s_to_01(s) for s in dps_s11])

        self.assertEqual(np.testing.assert_allclose(dps_s01n, self.dps_s01, 1E-06), None)

    def test_s_to_11_10_scale_equal(self):

        afi = self.af.copy()
        afi.scale(1.5)
        points = afi.points
        dps_s11 = np.array([afi.s_to_11(s) for s in self.dps_s01])
        afii = self.af.copy()
        afii.scale(1.5)
        pointsn = afii.points
        dps_s01n = np.array([afii.s_to_01(s) for s in dps_s11])

        self.assertEqual(np.testing.assert_allclose(dps_s01n, self.dps_s01, 1E-06), None)
        self.assertEqual(np.testing.assert_allclose(pointsn, points, 1E-06), None)

    def test_s_to_11_10_scale_not_equal(self):

        afi = self.af.copy()
        afi.scale(1.1)
        #points = afi.points
        dps_s11 = np.array([afi.s_to_11(s) for s in self.dps_s01])
        afii = self.af.copy()
        afii.scale(1.5)
        #pointsn = afii.points
        dps_s01n = np.array([afii.s_to_01(s) for s in dps_s11])

        self.assertEqual(np.testing.assert_allclose(dps_s01n, self.dps_s01, 1E-06), None)
        #self.assertEqual(np.testing.assert_allclose(pointsn, points, 1E-06), None)

class AirfoilShapeTests2(unittest.TestCase):
    ''' Tests for components.airfoil.py
    '''

    def test_redistribute_linear(self):

        af = AirfoilShape(points=np.loadtxt('data/ffaw3241.dat'))
        ds = np.array([ 0.11712651,  0.117116  ,  0.11720435,  0.11725923,  0.11719773,
        0.11713939,  0.11709662,  0.11696592,  0.11212361,  0.02098937,
        0.02108704,  0.02109582,  0.02110256,  0.0211067 ,  0.02110718,
        0.02110731,  0.02110809,  0.02110783,  0.02110793,  0.08436049,
        0.08439066,  0.08441201,  0.0844209 ,  0.08442478,  0.08442662,
        0.08442745,  0.08442748,  0.08442729,  0.08442751])
        af.redistribute(ni=30, dist=[[0, 0.01, 1],
                              [0.5, 0.01, 10], 
                              [0.6, 0.01, 20], 
                              [1, 0.01, 30]], linear=True)
        self.assertEqual(np.testing.assert_allclose(af.ds, ds, 1E-06), None)


    def test_redistribute_distfunc(self):

        af = AirfoilShape(points=np.loadtxt('data/ffaw3241.dat'))
        ds = np.array([ 0.03187173,  0.0662534 ,  0.1239282 ,  0.19230617,  0.22543051,
        0.19175545,  0.1237126 ,  0.06599569,  0.03104575,  0.02098937,
        0.02108704,  0.02109582,  0.02110256,  0.0211067 ,  0.02110718,
        0.02110731,  0.02110809,  0.02110783,  0.02110793,  0.02879817,
        0.05032088,  0.0816139 ,  0.1176486 ,  0.1436471 ,  0.1436658 ,
        0.11769158,  0.0816484 ,  0.05033041,  0.02879825])
        af.redistribute(ni=30, dist=[[0, 0.01, 1],
                              [0.5, 0.01, 10], 
                              [0.6, 0.01, 20], 
                              [1, 0.01, 30]])
        self.assertEqual(np.testing.assert_allclose(af.ds, ds, 1E-06), None)

if __name__ == '__main__':

    unittest.main()
