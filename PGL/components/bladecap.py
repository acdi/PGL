
import numpy as np
import math

from PGL.main.curve import Curve, Line
from PGL.main.coons import CoonsPatch
from PGL.main.bezier import BezierCurve
from PGL.main.domain import Domain, Block

class RootCap(object):
    """

    parameters
    ----------
    ni_root: int
        number of vertices in spanwise direction on fillet
    Fcap: float
        size of four block patch as a fraction of the root
        diameter, range 0 < Fcap < 1.
    Fblend: float
        factor controlling shape of four block patch.

        | Fblend => 0. takes the shape of tip_con,
        | Fblend => 1. takes the shape of a rectangle.
    direction: float
        Blade direction along z-axis: 1 positive z, -1 negative z
    """

    def __init__(self, **kwargs):

        self.cap_radius = 0.001
        self.Fcap = 0.95
        self.Fblend = 0.5
        self.ni_root = 8
        self.direction = 1.
        self.tip_con = np.array([])

        for k, w, in kwargs.iteritems():
            if k.startswith('cap_'):
                name = k[4:]
            else:
                name = k
            if hasattr(self, name):
                setattr(self, name, w)

        self.domain = Domain()

    def update(self):

        self.bsize = (self.tip_con.shape[0] - 1) / 4 + 1

        x = self.tip_con
        xt = np.zeros(x.shape)
        xt[:-32] = x[32:].copy()
        xt[-33:] = x[:33].copy()
        self.ni = x.shape[0]
        c = Curve(points=xt)
        self.base_cons = c.divide_connector(self.bsize)
        self.root_cons = []
        pcen = np.array([np.mean(self.tip_con[:, 0]),
                         np.mean(self.tip_con[:, 1]),
                         np.mean(self.tip_con[:, 2]) - self.cap_radius * self.direction])
        # print 'pcen', pcen
        self.collar_ps = []
        for i in range(4):
            p0 = x[32+i*(self.bsize-1),:]
            p1 = p0.copy()
            p1[2] += pcen[2]
            p2 = self.Fcap * p1 + (1. - self.Fcap) * pcen
            b = BezierCurve()
            b.add_control_point(p0)
            b.add_control_point(p1)
            b.add_control_point(p2)
            b.ni = self.ni_root
            b.update()
            b.redistribute(ni=self.ni_root)
            self.collar_ps.append(b.copy())
        self.collar_ps.append(self.collar_ps[0].copy())
        # create patch connectors

        self.patch_cons = []
        tcons = []
        for i in range(4):
                p0 = self.collar_ps[i].points[-1]
                p1 = self.collar_ps[i+1].points[-1]
                l = Line(p0, p1, ni=self.bsize)
                tcons.append(l.copy())
        for i in range(4):
                c = self.base_cons[i].copy()
                c.points[:,2] += pcen[2]
                c.points[:,:2] = self.Fcap * c.points[:,:2] + (1. - self.Fcap) * pcen[:2]
                c.points[:, :2] = self.Fblend*c.points[:, :2] + (1. - self.Fblend)*tcons[i].points[:, :2]
                c.initialize(c.points)
                self.patch_cons.append(c.copy())

        self.base_cons[2].points = self.base_cons[2].points[::-1].copy()
        self.base_cons[2].initialize(self.base_cons[2].points)
        self.base_cons[3].points = self.base_cons[3].points[::-1].copy()
        self.base_cons[3].initialize(self.base_cons[3].points)

        self.patch_cons[2].points = self.patch_cons[2].points[::-1].copy()
        self.patch_cons[2].initialize(self.patch_cons[2].points)
        self.patch_cons[3].points = self.patch_cons[3].points[::-1].copy()
        self.patch_cons[3].initialize(self.patch_cons[3].points)

        self.domain = Domain()

        p = CoonsPatch(ni=self.patch_cons[0].ni, nj=self.patch_cons[3].ni, block_name='cap_base-0')
        p.add_edge(0, self.patch_cons[0].copy())
        p.add_edge(1, self.patch_cons[2].copy())
        p.add_edge(2, self.patch_cons[3].copy())
        p.add_edge(3, self.patch_cons[1].copy())
        p.update()
        self.patches = []
        self.patches.append(p)
        p.P._flip_block()
        self.domain.add_blocks(p.P.split(33))
        for i in [0, 1]:
            p = CoonsPatch(ni=self.collar_ps[i].ni, nj=self.base_cons[i].ni, block_name = 'cap-0')
            p.add_edge(0, self.collar_ps[i].copy())
            p.add_edge(1, self.collar_ps[i+1].copy())
            p.add_edge(2, self.base_cons[i].copy())
            p.add_edge(3, self.patch_cons[i].copy())
            p.update()
            p.P._flip_block()
            self.patches.append(p)
            self.domain.add_blocks(p.P)
        for i in [2, 3]:
            p = CoonsPatch(ni=self.collar_ps[i+1].ni, nj=self.base_cons[i].ni, block_name = 'cap-2')
            p.add_edge(0, self.collar_ps[i+1])
            p.add_edge(1, self.collar_ps[i])
            p.add_edge(2, self.base_cons[i])
            p.add_edge(3, self.patch_cons[i])
            p.update()
            self.patches.append(p)
            self.domain.add_blocks(p.P)
        if self.direction == -1:
            self.domain.flip_all()

        self.domain.join_blocks('cap-0', 'cap-1', newname='root')
        self.domain.join_blocks('root', 'cap-2', newname='root')
        self.domain.join_blocks('root', 'cap-3', newname='root')
        bs = self.domain.blocks['root'].isplits((self.bsize - 1) / 2 + 1)
        del self.domain.blocks['root']
        self.domain.add_blocks(bs)
        for i in [7,6,5,4,3,2,1]:
            self.domain.join_blocks('root', 'root-%i' %i, newname='root')
